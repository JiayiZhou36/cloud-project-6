# Cloud_Project_6
By Jiayi Zhou
[![pipeline status](https://gitlab.com/JiayiZhou36/cloud-project-6/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/cloud-project-6/-/commits/main)

## Purpose of Project
This project instruments a Rust Lambda Function with Logging and Tracing. The function processes string data which removes leading and trailing whitespace in a string and return a formatted string.

## Requirements
* Add logging to a Rust Lambda function
* Integrate AWS X-Ray tracing
* Connect logs/traces to CloudWatch

## CloudWatch and X-Ray traces
![Screenshot_2024-03-06_at_11.37.06_PM](/uploads/ee1e111ea582a592011880b21f4fd2a4/Screenshot_2024-03-06_at_11.37.06_PM.png)

## Log
![Screenshot_2024-03-06_at_11.37.17_PM](/uploads/1df6d804f9b388045194ed542a07673e/Screenshot_2024-03-06_at_11.37.17_PM.png)

## Logging
![Screenshot_2024-03-07_at_12.02.30_AM](/uploads/7740e41d9f32548f2880103c2b8b410a/Screenshot_2024-03-07_at_12.02.30_AM.png)
