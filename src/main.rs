use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing::{error, info};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

#[derive(Debug, Deserialize)]
struct Request {
    command: String,
}

#[derive(Debug)]
pub struct StringTrim {
    pub trim_id: String,
    pub original_value: String,
    pub trimmed_value: String,
}

#[derive(Debug, Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let r_id = event.context.request_id;
    let command = event.payload.command; // Extract command from the request

    // Set up AWS DynamoDB client
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    // Create a StringTrim object with the command as the original_value
    let string_trim = StringTrim {
        trim_id: r_id.clone(),
        original_value: command.clone(),
        trimmed_value: command.trim().to_string(), // Trim the command
    };

    let trim_id = AttributeValue::S(string_trim.trim_id.clone());
    let original_value = AttributeValue::S(string_trim.original_value.clone());
    let trimmed_value = AttributeValue::S(string_trim.trimmed_value.clone());

    // Put item into DynamoDB
    if let Err(err) = client
        .put_item()
        .table_name("stringtrim")
        .item("trim_id", trim_id.clone())
        .item("original_value", original_value.clone())
        .item("trimmed_value", trimmed_value.clone())
        .send()
        .await
    {
        error!("Error while putting item: {:?}", err);
        return Err(err.into());
    }

    // Log the successful item insertion
    info!("Item successfully inserted into DynamoDB");

    // Create response
    let resp = Response {
        req_id: r_id,
        msg: format!(
            "Original value (command): '{}'. Trimmed value: '{}'.",
            string_trim.original_value, string_trim.trimmed_value
        ),
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Initialize logging
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env()
                .unwrap_or_else(|_| EnvFilter::new("info")),
        )
        .with_target(false)
        .without_time()
        .init();

    // Run Lambda function
    run(service_fn(function_handler)).await
}

#[cfg(test)]
mod tests {
    use super::*;
    use lambda_runtime::Context;

    #[tokio::test]
    async fn response_is_good_for_simple_input() {
        let id = "123456789".to_string();

        let mut context = Context::default();
        context.request_id = id.clone();

        let payload = Request {
            command: "X".to_string(),
        };
        let event = LambdaEvent { payload, context };

        let result = function_handler(event).await.unwrap();

        assert_eq!(result.req_id, id);
    }
}